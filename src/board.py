from car import Car
import itertools

class Board(object):
    """
    Represents a board state.
    """

    def __init__(self, w, h, cars=None):
        self.w = w
        self.h = h
        self.cars = cars.copy() if type(cars) is list else []

    def __str__(self):
        return "Board\n" + "".join("- " + str(car) + "\n" for car in self.cars)

    def __eq__(self, other):
        return self.w == other.w and \
            self.h == other.h and \
            self.cars == other.cars

    def copy(self):
        return Board(self.w, self.h, self.copy_cars())

    def copy_cars(self):
        result = []
        for car in self.cars:
            result.append(car.copy())
        return result

    def apply_move(self, move):
        """
        Destructively apply a (color, direction) move pair to the current
        board.
        """
        self.is_valid_move(move, raise_error=True, print_error=True)
        color, direction = move
        car = self.get_car(color)
        coord_map = {
                'L': (-1, 0),
                'R': (1, 0),
                'U': (0, -1),
                'D': (0, 1)
            }
        dx, dy = coord_map[direction]
        car.x += dx
        car.y += dy

    def move_applied(self, move):
        """
        Non-destructive version of apply_move. Returns a copy of the current
        board with the specified move applied to it.
        """
        board = self.copy()
        board.apply_move(move)
        return board

    def car_at(self, x, y):
        """
        Get the car at the position (x, y), or None if the position is empty.
        """
        for car in self.cars:
            if car.direction == 'x':
                if car.x <= x < car.x + car.length and y == car.y:
                    return car
            else:
                if x == car.x and car.y <= y < car.y + car.length:
                    return car
        return None

    def get_car(self, color):
        """
        Get a car by color, return None if color doesn't exist.
        """
        for car in self.cars:
            if car.color == color:
                return car
        return None

    @staticmethod
    def format_grid(grid):
        """
        Print an ASCII representation of the given grid.
        """
        result = ""
        for row in grid:
            for cell in row:
                result += "." if cell is None else cell.color
            result += "\n"
        return result

    def to_grid(self):
        """
        Denormalize the board into a grid representation.
        Returns a list of rows, each of which is either a Car object or None.
        This amounts to a 2D grid with row-first ordering, i.e., the correct
        indexing is grid[y][x], not grid[x][y].
        """
        grid = []
        for y in range(0, self.h):
            grid.append([])
            for x in range(0, self.w + 1):
                car = self.car_at(x, y)
                if car is not None or x < self.w:
                    grid[y].append(car)
        return grid

    def formatted(self):
        """
        Denormalize and format board into an ASCII grid representation.
        """
        return Board.format_grid(self.to_grid())

    @staticmethod
    def find_car_in(x, y, color, cars):
        """
        Find a car to which the position (x, y) can be appended, and
        where the color matches.
        """
        def is_match(car):
            if car.color != color:
                return False
            if car.length == 1 and \
                (
                  (car.x + car.length == x and car.y == y) or \
                  (car.x == x and car.y + car.length == y)
                ):
                return True
            if car.direction == 'x' and \
               car.x + car.length == x and \
               car.y == y:
               return True
            if car.direction == 'y' and \
               car.y + car.length == y and \
               car.x == x:
               return True
            return False
        matches = filter(is_match, cars)
        for result in matches:
            return result
        else:
            return None

    def find_car(self, x, y, color):
        """
        Non-static version of find_car_in, finds car in the current board.
        """
        return Board.find_car_in(x, y, color, self.cars)

    @staticmethod
    def parse(input):
        cars = []

        def extend_car(car, x, y):
            """
            Extend an existing car by altering its length such that it contains
            the specified coordinates.
            """
            if y > car.y:
                assert(car.x == x)
                assert(y == car.y + car.length)
                assert(car.direction == 'y' or car.length <= 1)
                car.direction = 'y'
                car.length = y - car.y + 1
            else:
                assert(car.y == y)
                assert(x == car.x + car.length)
                assert(car.direction == 'x' or car.length <= 1)
                car.direction = 'x'
                car.length = x - car.x + 1

        width = 0
        height = 0
        y = 0
        for row in input.splitlines():
            if row.strip() == "":
                continue
            x = 0
            for cell in row.strip():
                # skip space-ish characters
                if cell.strip == "":
                    continue
                if cell != ".":
                    car = Board.find_car_in(x, y, cell, cars)
                    if car is None:
                        cars.append(Car(cell, 'x', 1, x, y))
                    else:
                        extend_car(car, x, y)
                x += 1
                width = max(x, width)
            y += 1
            height = max(y, height)
        board = Board(width, height, cars)
        return board

    def enumerate_moves(self):
        """Enumerate all the potentially possible moves for this board.
        A move is encoded as a tuple (car color, direction), where the
        direction is a single character out of (L, R, U, D), indicating
        (left, right, up, down).
        No collision or bounds checks are performed.
        """
        moves = (car.enumerate_moves() for car in self.cars)
        return itertools.chain(*moves)

    def is_valid_move(self, move, raise_error=False, print_error=False):
        """Check whether the given move is valid. A move is invalid if any
        of the following is true:
        - it produces a collision
        - it causes a car to move out of the board (except for the 'r' car,
          which is allowed to move out to the right)
        - it causes a car to move laterally
        - it specifies a car that doesn't exist
        - it specifies an invalid movement direction.
        """
        class InvalidMoveException(Exception):
            pass

        try:
            color = move[0]
            direction = move[1]
            car = self.get_car(color)
            if car is None:
                raise InvalidMoveException("Car does not exist")

            # Cars can only move along their direction:
            if car.direction == 'x':
                if direction in ['U', 'D']:
                    raise InvalidMoveException("Car cannot move to the side")
            else:
                if direction in ['L', 'R']:
                    raise InvalidMoveException("Car cannot move to the side")

            # Determine coordinates of cell to be occupied:
            if direction == 'L':
                check_x = car.x - 1
                check_y = car.y
            elif direction == 'R':
                check_x = car.x + car.length
                check_y = car.y
            elif direction == 'U':
                check_x = car.x
                check_y = car.y - 1
            elif direction == 'D':
                check_x = car.x
                check_y = car.y + car.length
            else:
                raise InvalidMoveException("Invalid direction specifier " + direction)

            # Special case: the 'r' car can be moved out of the board boundaries
            # to the right.
            if car.color == 'r' and check_x == self.w:
                return True

            # Boundary checks:
            if check_x >= self.w or check_x < 0 or \
               check_y >= self.h or check_y < 0:
                raise InvalidMoveException("Board boundary exceeded")
            # Collision check:
            check_car = self.car_at(check_x, check_y)
            if not check_car is None:
                raise InvalidMoveException("Collision with car " + check_car.color)
            return True
        except InvalidMoveException as e:
            if print_error:
                print(e)
            if raise_error:
                raise
            else:
                return False

    def get_valid_moves(self):
        """Enumerate all the valid moves from the current board position.
        """
        all_moves = self.enumerate_moves()
        return filter(
            lambda move: self.is_valid_move(move),
            all_moves)

def test_car_at():
    board = Board(10, 10,
        [ Car('A', 'x', 2, 0, 0)
        , Car('B', 'y', 3, 1, 1)
        ])
    assert board.car_at(1, 2).color == 'B'
    assert board.car_at(2, 2) is None

def test_get_car():
    car = Car('A', 'x', 2, 0, 0)
    board = Board(3, 3, [ Car('B', 'x', 1, 1, 1),
                          car,
                          Car('X', 'y', 1, 5, 6)
                        ])
    found = board.get_car('A')
    assert found is car

def test_to_grid_empty():
    board = Board(2, 2, [])
    expected = [
            [None, None],
            [None, None]
        ]
    actual = board.to_grid()
    assert expected == actual

def test_to_grid_one_car():
    car = Car('A', 'x', 2, 0, 0)
    board = Board(2, 2, [ car ])
    expected = [
                   [car, car],
                   [None, None]
               ]
    actual = board.to_grid()
    assert expected == actual

def test_format_grid():
    car = Car('A', 'x', 2, 0, 0)
    grid = [
               [car, car],
               [None, None]
           ]
    expected = "AA\n..\n"
    actual = Board.format_grid(grid)
    assert expected == actual

def test_formatted():
    src = "AA\n..\nBB\n"
    board = Board.parse(src)
    actual = board.formatted()
    assert actual == src

def test_parse_empty():
    input = """
            ..
            ..
            """
    board = Board.parse(input)
    assert type(board) is Board
    assert board.w == 2
    assert board.h == 2
    assert board.car_at(0, 0) is None
    assert board.car_at(1, 0) is None
    assert board.car_at(0, 1) is None
    assert board.car_at(1, 1) is None

def test_parse_single():
    input = """
            ..
            AA
            """
    board = Board.parse(input)
    assert type(board) is Board
    assert board.w == 2
    assert board.h == 2
    assert board.car_at(0, 0) is None
    assert board.car_at(1, 0) is None
    assert board.car_at(0, 1).color == 'A'
    assert board.car_at(1, 1).color == 'A'
    assert len(board.cars) == 1

def test_parse_multi():
    input = """
            ..B.
            AAB.
            ..B.
            """
    board = Board.parse(input)
    assert type(board) is Board
    assert board.w == 4
    assert board.h == 3
    assert board.car_at(0, 0) is None
    assert board.car_at(1, 0) is None
    assert board.car_at(2, 0).color == 'B'
    assert board.car_at(0, 1).color == 'A'
    assert board.car_at(1, 1).color == 'A'
    assert board.car_at(2, 1).color == 'B'
    assert board.car_at(0, 2) is None
    assert board.car_at(1, 2) is None
    assert board.car_at(2, 2).color == 'B'
    assert len(board.cars) == 2

def test_parse_many():
    input = """
            ..B.C
            AAB.C
            ..BEE
            """
    board = Board.parse(input)
    assert type(board) is Board
    assert board.w == 5
    assert board.h == 3
    assert board.car_at(0, 0) is None
    assert board.car_at(1, 0) is None
    assert board.car_at(2, 0).color == 'B'
    assert board.car_at(0, 1).color == 'A'
    assert board.car_at(1, 1).color == 'A'
    assert board.car_at(2, 1).color == 'B'
    assert board.car_at(0, 2) is None
    assert board.car_at(1, 2) is None
    assert board.car_at(2, 2).color == 'B'
    assert board.car_at(4, 2).color == 'E'
    assert len(board.cars) == 4

def test_enumerate_moves():
    cars = [ Car('A', 'x', 1, 1, 1),
             Car('B', 'y', 1, 2, 3)
           ]
    board = Board(2, 2, cars)
    actual = list(board.enumerate_moves())
    print(actual)
    expected = [('A', 'L'), ('A', 'R'), ('B', 'U'), ('B', 'D')]
    assert actual == expected

def test_is_valid_move():
    src = """
          .AA.C
          BB..C
          ...rr
          .DDEE
          """
    board = Board.parse(src)
    print(board.formatted())
    assert board.is_valid_move(('A', 'R'))
    assert board.is_valid_move(('A', 'L'))
    assert not board.is_valid_move(('A', 'U'))
    assert not board.is_valid_move(('A', 'D'))

    assert not board.is_valid_move(('B', 'L'))
    assert board.is_valid_move(('B', 'R'))
    assert not board.is_valid_move(('B', 'U'))
    assert not board.is_valid_move(('B', 'D'))

    assert board.is_valid_move(('r', 'R'))

def test_get_valid_moves():
    src = """
          .AA.
          ...E
          ...E
          ..rr
          BB..
          """
    board = Board.parse(src)
    moves = set(board.get_valid_moves())
    print(board.formatted())
    print(list(moves))
    assert ('A', 'L') in moves
    assert ('A', 'R') in moves
    assert ('E', 'U') in moves
    assert ('r', 'R') in moves
    assert ('r', 'L') in moves
    assert not ('E', 'D') in moves

def test_apply_move():
    src = """
          .AA.
          ...E
          ...E
          ..rr
          BB..
          """
    board = Board.parse(src)
    board.apply_move(('A', 'R'))
    assert board.car_at(3, 0).color == 'A'
    assert board.car_at(1, 0) is None

def test_move_applied():
    src = """
          .AA.
          ...E
          ...E
          ..rr
          BB..
          """
    board = Board.parse(src)
    new_board = board.move_applied(('A', 'R'))
    assert new_board.car_at(3, 0).color == 'A'
    assert new_board.car_at(1, 0) is None
    assert board != new_board

def test_eq():
    src = """
          .AA
          ...
          ..E
          """
    a = Board.parse(src)
    b = Board.parse(src)
    c = a.copy()
    d = Board.parse("...\n...\n...\n")
    assert a == b
    assert a == c
    assert a != d
