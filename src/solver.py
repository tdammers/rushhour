from board import Board
from solution import Solution
from heapq import heappush, heappop

class Solver(object):
    """
    A naive implementation of A*, applied to the Rush Hour problem.
    See https://en.wikipedia.org/wiki/A-star.
    """
    def __init__(self, board, debug=False):
        assert type(board) is Board
        self.board = board
        self.open_set = []
        heappush(self.open_set, Solution(board))
        self.closed_set = dict()
        self.solution = None
        self.debug = debug

    def step(self):
        """
        Perform one search iteration. Each iteration pops one candidate from
        the open set, checks whether it is worth investigating, and if so,
        generates all the valid moves from there and pushes them back onto the
        open set. If a solution is found, short-circuit.
        """
        # Get the best partial solution from the open set
        if len(self.open_set) == 0:
            return # Nothing to be done anymore
        current = heappop(self.open_set)
        if self.debug:
            print("Open: {0}; closed: {1}".format(
                len(self.open_set),
                len(self.closed_set)))
            print(current.moves)
            print("Current: {0}+{1}={2}".format(
                current.get_steps(),
                current.get_heuristic(),
                current.get_score()))

        # Have we found a solution?
        if current.get_heuristic() == 0:
            # Found the solution!
            self.solution = current
            return

        # Check against the closed set to avoid loops and redundant solutions
        key = current.board.formatted()
        if key in self.closed_set:
            other = self.closed_set[key]
            if current.get_score() >= other.get_score():
                return # Skip this one: we already know a faster way here
        # This is the fastest way to get to this particular board position
        self.closed_set[key] = current

        # Enumerate valid moves from this position, add to open set
        moves = list(current.board.get_valid_moves())
        for move in moves:
            node = current.move_appended(move)
            heappush(self.open_set, node)

    def solve(self):
        """
        Attempt to completely solve by iterating until either the open set is
        empty (which means no solutions exist), or a winning solution was found
        (which, due to the characteristics of Dijkstra's / A* means that the
        found solution is the best possible solution).
        """
        while self.solution is None and len(self.open_set) > 0:
            self.step()
        return self.solution

def test_solvable():
    src = """
          ...A
          .rrA
          ..BB
          ....
          """
    board = Board.parse(src)
    solver = Solver(board)
    solution = solver.solve()
    assert solution
    # This is the correct solution; any other valid solution requires
    # more moves, so we want the algorithm to find this exact one.
    assert solution.moves == \
        [('B', 'L'),
         ('A', 'D'),
         ('A', 'D'),
         ('r', 'R'),
         ('r', 'R')]

def test_unsolvable():
    src = """
          ...A
          .rrA
          .CBB
          .C..
          """
    board = Board.parse(src)
    solver = Solver(board)
    solution = solver.solve()
    assert solution is None
