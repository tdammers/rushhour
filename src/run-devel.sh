#!/usr/bin/zsh

for ((;;))
do
    ctags -R
    nosetests3 **/*.py
    ./main.py
    inotifywait -e modify **/*.py
done
