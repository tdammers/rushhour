class Car(object):
    """
    Represents a car. Cars have a color, a position, a direction, and a length.
    The position indicates the top-left corner of the vehicle.
    """
    def __init__(self, color, direction, length, x, y):
        self.color = color
        self.direction = direction
        self.length = length
        self.x = x
        self.y = y
        self._assert_invariants()

    def __eq__(self, other):
        return self.color == other.color and \
               self.direction == other.direction and \
               self.x == other.x and \
               self.y == other.y

    def copy(self):
        return Car(self.color, self.direction, self.length, self.x, self.y)

    def _assert_invariants(self):
        assert type(self.color) is str
        assert len(self.color) == 1
        assert self.direction in ['x', 'y']
        assert type(self.length) is int
        assert self.length > 0
        assert type(self.x) is int
        assert type(self.y) is int

    def __str__(self):
        return "Car(" + \
               ", ".join(
                        [ self.color,
                          self.direction,
                          str(self.length),
                          str(self.x),
                          str(self.y)
                        ]) + \
               ")"

    def enumerate_moves(self):
        """Enumerate all the potentially possible moves for this car.
        A move is encoded as a tuple (car color, direction), where the
        direction is a single character out of (L, R, U, D), indicating
        (left, right, up, down).
        No collision or bounds checks are performed.
        """
        self._assert_invariants()
        if self.direction == 'x':
            return [ (self.color, "L"), (self.color, "R") ]
        else:
            return [ (self.color, "U"), (self.color, "D") ]

def test_car_str():
    car = Car('A', 'x', 2, 3, 4)
    expected = "Car(A, x, 2, 3, 4)"
    actual = str(car)
    assert actual == expected

def test_enumerate_moves_x():
    car = Car('A', 'x', 1, 0, 0)
    expected = [('A', 'L'), ('A', 'R')]
    actual = car.enumerate_moves()
    assert actual == expected

def test_enumerate_moves_x():
    car = Car('A', 'y', 1, 0, 0)
    expected = [('A', 'U'), ('A', 'D')]
    actual = car.enumerate_moves()
    assert actual == expected

def test_cars_eq():
    a = Car('A', 'y', 1, 2, 3)
    b = Car('A', 'y', 1, 2, 3)
    c = Car('A', 'x', 1, 2, 3)
    assert a == b
    assert a != c
