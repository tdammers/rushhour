from board import Board
from car import Car

class Solution(object):
    def __init__(self, board, moves=None):
        self.board = board
        self.moves = [] if moves is None else moves.copy()

    def copy(self):
        return Solution(self.board.copy(), self.moves.copy())

    def __eq__(self, other):
        return self.board == other.board and \
               self.moves == other.moves

    def __gt__(self, other):
        return self.get_score() > other.get_score()

    def __ge__(self, other):
        return self.get_score() >= other.get_score()

    def __lt__(self, other):
        return self.get_score() < other.get_score()

    def __le__(self, other):
        return self.get_score() <= other.get_score()

    def __ne__(self, other):
        return not self == other

    def append_move(self, move):
        """
        Execute and append a move.
        """
        self.board.apply_move(move)
        self.moves.append(move)

    def move_appended(self, move):
        """
        Non-destructive version of append_move.
        """
        solution = self.copy()
        solution.append_move(move)
        return solution

    def get_steps(self):
        """
        Gets the number of moves made so far.
        """
        return len(self.moves)

    def get_heuristic(self):
        """
        Gets the minimum number of moves required to achieve a winning position
        ('r' car moved out to the right)
        """
        car = self.board.get_car('r')
        if car is None:
            raise Exception("No 'r' car in board, unsolvable!")
        assert car.direction == 'x'
        car_right = car.x + car.length
        if car_right > self.board.w:
            return 0 # Already a winning position
        else:
            # obstacles = 0
            # for x in range(car_right, self.board.w):
            #     if self.board.car_at(x, car.y) is not None:
            #         obstacles += 1
            # return self.board.w - car_right + 1 + obstacles
            return self.board.w - car_right + 1

    def get_score(self):
        """
        Gets the score for the current board position, which is the number of
        steps taken so far plus the minimum number of moves towards a winning
        position, i.e., get_steps() + get_heuristic().
        """
        return self.get_steps() + self.get_heuristic()

def test_eq():
    src = """
          AA.
          ...
          """
    board = Board.parse(src)
    a = Solution(board)
    b = a.copy()
    assert a == b

def test_append_move():
    src = """
          AA.
          .BB
          """
    dst = """
          .AA
          .BB
          """
    move = ('A', 'R')
    expected = Solution(Board.parse(dst), [move])
    actual = Solution(Board.parse(src)).move_appended(move)
    assert actual == expected

def test_steps():
    src = """
          AA.
          .BB
          """
    solution = Solution(Board.parse(src)) \
        .move_appended(('A','R')) \
        .move_appended(('B','L'))
    assert solution.get_steps() == 2

def test_heuristic_pos():
    src = """
          AA.
          rr.
          ...
          """
    board = Board.parse(src)
    solution = Solution(board)
    heuristic = solution.get_heuristic()
    assert heuristic == 2

def test_heuristic_zero():
    src = """
          AA.
          .rr
          ...
          """
    board = Board.parse(src)
    solution = Solution(board)
    solution.append_move(('r', 'R'))
    heuristic = solution.get_heuristic()
    assert heuristic == 0
