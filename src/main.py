#!/usr/bin/env python3

from board import Board
from car import Car
from solver import Solver
import sys
import time

def format_move(move):
    color, direction = move
    direction_symbols = {
            'U': '↑',
            'D': '↓',
            'L': '←',
            'R': '→'
        }
    direction_symbol = direction_symbols[direction]
    return "{0}{1}".format(color, direction_symbol)

def print_moves(moves):
    print(", ".join(format_move(move) for move in moves))

def print_solution(solution, config):
    if solution is None:
        print("Not solvable.")
        return
    print("Solved in {0} {1}:".format(
        len(solution.moves),
        "move" if len(solution.moves) == 1 else "moves"))
    print_moves(solution.moves)
    print(solution.board.formatted())

def animate_solution(board, solution, config):
    def escseq(s):
        return chr(27) + s
    def print_board(board):
        print("{0}{1}".format(
            escseq("[H"),
            board.formatted()))
    board = board.copy()
    print(escseq("[2J"))
    print_board(board)
    moves = []
    for move in solution.moves:
        time.sleep(config.animation_delay)
        board.apply_move(move)
        moves.append(move)
        print_board(board)
        print_moves(moves)

def process_file(input_file, config):
    """
    Process one input file.
    """
    if type(input_file) is str:
        print("*** " + input_file)
        with open(input_file, "r") as f:
            src = f.read()
    else:
        print("*** " + input_file)
        src = input_file.read()
    board = Board.parse(src)
    print(board.formatted())
    solver = Solver(board, debug=config.debug)
    solution = solver.solve()
    if config.animate:
        animate_solution(board, solution, config)
    else:
        print_solution(solution, config)

class Config(object):
    """
    Program configuration object.
    """
    def __init__(self):
        self.input_files = []
        self.debug = False
        self.animate = False
        self.animation_delay = 0.5

    def add_input_file(self, input_file):
        self.input_files.append(input_file)

def parse_args(args):
    """
    Parse a list of arguments into a configuration object.
    """
    config = Config()
    args.pop(0)
    while len(args) > 0:
        arg = args.pop(0)
        if arg.startswith("-"):
            if arg == "-":
                config.add_input_file(sys.stdin)
            elif arg == "-debug":
                config.debug = True
            elif arg == "-animate":
                config.animate = True
            elif arg == "-delay":
                config.animation_delay = float(args.pop(0))
            else:
                raise Exception("Invalid argument: " + arg)
        else:
            config.add_input_file(arg)
    return config

def main(args):
    try:
        config = parse_args(args)
        for input_file in config.input_files:
            process_file(input_file, config)
    except Exception as e:
        print(str(e))

if __name__ == '__main__':
    main(sys.argv)
